==================
django-easyoptions
==================

Easily add more ``Meta``\esque option classes to your Django forms, models and more.

Installing
==========

Install using pip::

    pip install django-easyoptions

It works with Django 1.4 and upwards.

Using
=====

To make use of Options in your class,
define an Options class, and
use one of the metaclass factories provided.

.. code:: python

    from django import forms
    from django.utils import six
    from easyoption.options import OptionsBase, form_option_factory


    # Define your ``Option``\s container class
    class ExtendedOptions(OptionsBase):
        def __init__(self, options):

            # Collect your options, supplying defaults as appropriate
            self.foo = options.pop('foo', None)
            self.frobnicate = options.pop('frobnicate', True)

            # Call the ``super().__init__()``, which ensures all options are
            # consumed.
            super(ExtraOption, self).__init__(options)


    # Define your base class, using the options from ``_extendedoptions``
    class ExtendedForm(six.with_metaclass(
            form_option_factory(ExtendedOptions),
            forms.Form)):

        def __init__(self, **kwargs):
            super(ExtendedForm, self).__init__(**kwargs)

            if self._extendedoptions.frobnicate:
                self.bar(self._extendedoptions.foo)


    # Extend your base class, and define the ``ExtendedOptions`` for options
    # for this specific implementation
    class SpecificExtendedForm(ExtendedForm):
        class ExtendedOptions:
            foo = 'quux'

Methods
-------

``options.options_factory``
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The main method that does all of the work is
``options.options_factory``.
It generates a new Metaclass that you should apply to your base class,
allowing subclasses to define their own options.
The method accepts four arguments:

``options_processor``
    A callable that processes options the options dict from the class.
    This is usually a class subclassing ``options.OptionsBase``,
    but could be any callable as long as it returns some options structure.
``options_class_name``
    The name of the attribute on the class where the options definition can be found.

    Defaults to the ``options_processor.__name__``, which works well with classes.
``options_attr_name``
    The name of the attribute where processed options will be stored.

    Defaults to ``'_' + options_class_name.lower()``.
``metaclass``
    The base metaclass to extend.
    Many Django classes have metaclasses already,
    so if you're adding options to class that already has a metaclass,
    you must supply it here for the class to work properly.

    Defaults to ``type``.

``options.form_options_factory``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calls ``options.options_factory``
with the ``metaclass`` already set to the metaclass for Django forms.

``options.modelform_options_factory``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calls ``options.options_factory``
with the ``metaclass`` already set to the metaclass for Django model forms.