#!/usr/bin/env python

import sys
from django.conf import settings
from django.core.management import call_command

def main():

    settings.configure(
        DEBUG=True,
        ROOT_URLCONF='',
        INSTALLED_APPS=('easyoptions',),
        DATABASES={'default': {'ENGINE': 'django.db.backends.sqlite3'}})

    call_command('test')

if __name__ == '__main__':
    main()
